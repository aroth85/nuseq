import yaml

def main(args):
    with open(args.in_file) as fh:
        parser = TrainingFileParser(fh)
        
        results = [x for x in parser]

    with open(args.out_file, 'w') as out_fh:
        yaml.dump(results, out_fh, default_flow_style=False)

class TrainingFileParser(object):
    def __init__(self, fh):
        self.fh = fh
        
        self.line = self.fh.readline()
        
        self.finished = False
        
    def __iter__(self):
        return self
    
    def next(self):
        if self.finished or (self.line.strip() == ''):
            raise StopIteration()
        
        result = {'header' : {}, 'data' : []}
        
        positions = set()
        
        try:
            while self.line.startswith('#') and (self.line.strip() != ''):
                result['header'].update(self._parse_header_line())

                self.line = self.fh.readline()

            while not self.line.startswith('#') and (self.line.strip() != ''):
                pos = self._parse_positions_line()
                
                if (pos['chrom'], pos['coord']) not in positions:
                    result['data'].append(pos)
                    
                    positions.add((pos['chrom'], pos['coord']))

                self.line = self.fh.readline()
            
            return result
            
        except StopIteration:
            self.finished = True
            return result
            
    def _parse_header_line(self):
        line = self.line[1:].strip()
            
        sample, path = line.split()
            
        return {sample : path}
    
    def _parse_positions_line(self):
        chrom, coord, status = self.line.strip().split()
        
        return {'chrom' : chrom, 'coord' : int(coord), 'status' : status.lower()}
    
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser(description='''Convert mutationSeq training data file to YAML file.''')
    
    parser.add_argument('--in_file', required=True)
    
    parser.add_argument('--out_file', required=True)
    
    args = parser.parse_args()
    
    main(args)
# Cython imports
from libc.math cimport log
from pysam.libcfaidx cimport FastaFile

cimport cython
cimport numpy as np

# Python imports
from collections import OrderedDict

import array
import numpy as np
import pandas as pd

# Local Cython imports
from .constants cimport C_NUCLEOTIDES, NUCLEOTIDES, NUM_NUCLEOTIDES, LOG_NUM_NUCLEOTIDES, C_INDEX, G_INDEX
from .core cimport FeatureExtractor

cdef class FastaFeatureExtractor(FeatureExtractor):
    cdef FastaFile fasta

    cdef HomoPolymerCounter hp_counter

    def __init__(self, fasta):
        self.fasta = fasta

        self.hp_counter = HomoPolymerCounter(self.fasta)

    def extract_features_from_coords(self, chrom, beg, end, window_size=500):
        '''
        0 based coordinates
        '''
        ref_length = self.fasta.get_reference_length(chrom)

        min_index = 0

        max_index = ref_length

        if beg < min_index:
            print 'Beginning index {0} is less than {1}. Setting to {1}.'.format(beg, min_index)

            beg = min_index

        if end > max_index:
            print 'End index {0} is greater than {1}, the length of the chromosome. Setting to {1}.'.format(
                end,
                max_index
            )

            end = max_index

        if (end < min_index) or (beg > max_index) or ((end - beg) <= 0):
            return None

        entropy, gc_content = compute_entropy_and_gc_content(self.fasta, chrom, beg, end, window_size)

        df = pd.DataFrame(OrderedDict((
            ('ref_base', list(self.fasta.fetch(chrom, beg, end))),
            ('ref_genome_entropy', entropy),
            ('gc_content', gc_content),
            ('forward_homopolymer_length', self.hp_counter.get_forward_counts(chrom, beg, end)),
            ('reverse_homopolymer_length', self.hp_counter.get_reverse_counts(chrom, beg, end))
        )))

        df.insert(0, 'chrom', chrom)

        df.insert(1, 'coord', pd.np.arange(beg, end))

        df['chrom'] = df['chrom'].astype(str)

        return df

cdef class SlidingWindowCounter:
    cdef np.uint64_t[:] counts
    cdef unsigned int pos
    cdef char * seq
    cdef Py_ssize_t seq_len
    cdef unsigned int window_beg
    cdef unsigned int window_end
    cdef unsigned int window_size

    def __init__(self, seq, window_size):
        self.counts = np.zeros(4, dtype=np.uint64)

        self.pos = 0

        self.seq = < bytes > seq

        self.seq_len = len(seq)

        self.window_size = window_size

        self._update_window_coords()

    def __iter__(self):
        return self

    def __next__(self):
        self.cnext()

        return self.counts

    cdef cnext(self):
        if self.pos == 0:
            self._init_counts()

        elif self.pos >= self.seq_len - self.window_size:
            raise StopIteration()

        else:
            self._update_counts()

        self.pos += 1

        self._update_window_coords()

    cdef _init_counts(self):
        cdef char base
        cdef int n
        cdef int window_index

        for window_index in range(self.window_beg, self.window_end):
            base = self.seq[window_index]

            for n in range(NUM_NUCLEOTIDES):
                if base == C_NUCLEOTIDES[n]:
                    self.counts[n] += 1

    cdef _update_counts(self):
        cdef char base
        cdef int n

        # Decrement from beginning
        base = self.seq[self.window_beg - 1]

        for n in range(NUM_NUCLEOTIDES):
            if base == C_NUCLEOTIDES[n]:
                self.counts[n] -= 1

        # Increment from end
        base = self.seq[self.window_end - 1]

        for n in range(NUM_NUCLEOTIDES):
            if base == C_NUCLEOTIDES[n]:
                self.counts[n] += 1

    cdef _update_window_coords(self):
        self.window_beg = self.pos

        self.window_end = self.pos + self.window_size + 1


@cython.cdivision(True)
cdef tuple compute_entropy_and_gc_content(FastaFile fasta, cython.str chrom, int beg, int end, int window_size):
    cdef SlidingWindowCounter counter
    cdef np.uint64_t[:] counts
    cdef cython.str seq
    cdef int beg_pad, end_pad, index, window_beg, window_end, total
    cdef float prob

    cdef int half_window_size = window_size // 2
    cdef int region_length = end - beg
    cdef int ref_length = fasta.get_reference_length(chrom)

    cdef np.float64_t[:] entropy = np.zeros(region_length, dtype=np.float64)
    cdef np.float64_t[:] gc_content = np.zeros(region_length, dtype=np.float64)

    window_beg = beg - half_window_size

    window_end = end + half_window_size

    if window_beg < 0:
        beg_pad = abs(window_beg)

        window_beg = 0

    else:
        beg_pad = 0

    if window_end > ref_length:
        end_pad = window_end - ref_length

        window_end = ref_length

    else:
        end_pad = 0

    seq = fasta.fetch(chrom, window_beg, window_end)

    seq = 'N' * beg_pad + seq + 'N' * end_pad

    counter = SlidingWindowCounter(seq, window_size)

    for index in range(region_length):
        counter.cnext()

        counts = counter.counts

        total = 0

        for n in range(NUM_NUCLEOTIDES):
            total += counts[n]

        if total > 0:
            for n in range(NUM_NUCLEOTIDES):
                prob = counts[n] / < float > total

                if prob > 0:
                    entropy[index] -= (log(prob) / LOG_NUM_NUCLEOTIDES) * prob

            gc_content[index] = (counts[C_INDEX] + counts[G_INDEX]) / < float > total

    return entropy, gc_content

cdef class FastaFileProxy:
    cdef FastaFile fasta

    def __init__(self, fasta):
        self.fasta = fasta

    cdef unsigned int get_reference_length(self, cython.str chrom):
        return self.fasta.get_reference_length(chrom)

    cpdef cython.str fetch(self, cython.str chrom, int beg, int end):
        pass

cdef class FastaFileForwardProxy(FastaFileProxy):
    cpdef cython.str fetch(self, cython.str chrom, int beg, int end):
        return self.fasta.fetch(chrom, beg, end)

cdef class FastaFileReverseProxy(FastaFileProxy):
    cpdef cython.str fetch(self, cython.str chrom, int beg, int end):
        cdef unsigned int ref_length = self.get_reference_length(chrom)

        cdef unsigned int rev_beg = ref_length - end

        cdef unsigned int rev_end = ref_length - beg

        return self.fasta.fetch(chrom, rev_beg, rev_end)[::-1]


cdef class HomoPolymerCounter(object):
    cdef FastaFile fasta

    cdef dict _seq_cache

    def __init__(self, fasta):
        self.fasta = fasta

        self._seq_cache = {}

    def get_seq(self, chrom):
        if chrom not in self._seq_cache:
            self._seq_cache = {}

            self._seq_cache[chrom] = self.fasta.fetch(chrom)

        return self._seq_cache[chrom]

    def get_forward_counts(self, chrom, beg, end):
        beg, end = self._check_beg_end(chrom, beg, end)

        chrom_len = self.fasta.get_reference_length(chrom)

        # Init for edge effect
        count = 1

        seq = self.get_seq(chrom)

        last_base = seq[end - 1]

        while True:
            seq_pos = end - 1 + count

            if seq_pos >= chrom_len:
                break

            if last_base == seq[seq_pos]:
                count += 1

            else:
                break

        # Count rest
        seq = seq[beg:end][::-1]

        return self._get_counts(count, seq)[::-1]

    def get_reverse_counts(self, chrom, beg, end):
        beg, end = self._check_beg_end(chrom, beg, end)

        # Init for edge effect
        count = 1

        seq = self.get_seq(chrom)

        first_base = seq[beg]

        while True:
            seq_pos = beg - count

            if seq_pos < 0:
                break

            if first_base == seq[seq_pos]:
                count += 1

            else:
                break

        # Count rest
        seq = seq[beg:end]

        return self._get_counts(count, seq)

    def _check_beg_end(self, chrom, beg, end):
        chrom_len = self.fasta.get_reference_length(chrom)

        if beg < 0:
            beg = 0

        if end > chrom_len:
            end = chrom_len

        return beg, end

    def _get_counts(self, init_count, seq):
        counts = []

        count = init_count

        for i in range(len(seq) - 1):
            counts.append(count)

            if seq[i] == seq[i + 1]:
                count += 1

            else:
                count = 1

        counts.append(count)

        return np.array(counts)

'''
Created on Nov 13, 2015

@author: Andrew Roth
'''
from __future__ import division

import numpy as np
import pandas as pd

from .bam import BamFeatureExtractor
from .core import FeatureExtractor
from .data_frame import get_alt_base, get_base_values
from .fasta import FastaFeatureExtractor
from .math import compute_multinomial_emission, compute_xentropy

nucleotides = ['A', 'C', 'G', 'T']


class SingleSampleFeatureExtractor(FeatureExtractor):

    def __init__(
            self,
            bam,
            ref_genome_fasta,
            min_depth=0):

        self.bam_extractor = BamFeatureExtractor(bam)

        self.ref_genome_fasta_extractor = FastaFeatureExtractor(ref_genome_fasta)

        self.min_depth = min_depth

    def extract_features_from_position_list(self, positions):
        ref_df = []

        bam_df = []

        for chrom, coord in positions:
            bam_df.append(self.bam_extractor.extract_features_from_position(chrom, coord))

            ref_df.append(self.ref_genome_fasta_extractor.extract_features_from_position(chrom, coord))

        bam_df = pd.concat(bam_df)

        ref_df = pd.concat(ref_df)

        return self._compute_df(bam_df, ref_df)

    def extract_features_from_coords(self, chrom, beg, end):
        bam_df = self.bam_extractor.extract_features_from_coords(chrom, beg, end)

        if bam_df is None or bam_df.empty:
            return None

        ref_df = self.ref_genome_fasta_extractor.extract_features_from_coords(chrom, beg, end)

        return self._compute_df(bam_df, ref_df)

    def _compute_df(self, bam_df, ref_df):
        if (bam_df is None) or (ref_df is None):
            return None

        bam_df = bam_df[bam_df['total_counts'] >= self.min_depth]

        if bam_df.empty:
            return None

        # Add ref base info
        df = pd.merge(ref_df, bam_df, on=['chrom', 'coord'])

        # Get alt bases
        alt_base = get_alt_base(
            df['ref_base'].values,
            'ACGT',
            df[['A_counts', 'C_counts', 'G_counts', 'T_counts']].values
        )

        # Add alt base information
        df.insert(3, 'alt_base', alt_base)

        # Compute derived BAM features
        df = self._compute_single_sample_features(df)

        df = df.reset_index()

        df = df.drop('index', axis=1)

        df = df.set_index(['chrom', 'coord', 'ref_base', 'alt_base'])

        df = df.astype(pd.np.float64)

        df = df.reset_index()

        return df

    def _compute_single_sample_features(self, df, eps=1e-5):
        _convert_to_ref_alt(df)

        ignore_cols = [
            'chrom',
            'coord',
            'ref_base',
            'alt_base',
            'ref_genome_entropy',
            'gc_content',
            'forward_homopolymer_length',
            'reverse_homopolymer_length'
        ]

        for feature in df.columns:
            if feature in ignore_cols:
                continue

            if feature != 'total_counts':
                derived_feature = feature + '_total_counts_ratio'

                df[derived_feature] = df[feature] / df['total_counts']

        return df


def _convert_to_ref_alt(df):
    for allele in ['ref', 'alt']:
        bases = df['{0}_base'.format(allele)].values

        for feature in ['forward_counts', 'reverse_counts', 'bqual', 'distance', 'mqual', 'counts']:
            key = '{0}_{1}'.format(allele, feature)

            cols = []

            for base in nucleotides:
                cols.append('{0}_{1}'.format(base, feature))

            values = df[cols].values.astype(np.uint64)

            df[key] = get_base_values(bases, values)

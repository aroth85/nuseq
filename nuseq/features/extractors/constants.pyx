from libc.math cimport log

cpdef cython.str NUCLEOTIDES = 'ACGT'
cpdef Py_ssize_t NUM_NUCLEOTIDES = len(NUCLEOTIDES)
cpdef float LOG_NUM_NUCLEOTIDES = log(NUM_NUCLEOTIDES)
cdef unsigned A_INDEX = NUCLEOTIDES.index('A')
cdef unsigned C_INDEX = NUCLEOTIDES.index('C')
cdef unsigned G_INDEX = NUCLEOTIDES.index('G')
cdef unsigned T_INDEX = NUCLEOTIDES.index('T')

cdef char * C_NUCLEOTIDES = <bytes > NUCLEOTIDES

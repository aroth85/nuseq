cdef class FeatureExtractor(object):
    def extract_features_from_coords(self, chrom, beg, end, **kwargs):
        '''
        0-based half open indexing.
        '''
        raise NotImplemented

    def extract_features_from_position(self, chrom, coord, **kwargs):
        '''
        1-based indexing for a single position.
        '''
        chrom = str(chrom)

        coord = int(coord)

        df = self.extract_features_from_coords(chrom, coord - 1, coord)

        if df is None:
            return None

        df['coord'] = df['coord'] + 1

        return df

    def extract_features_from_region(self, region, **kwargs):
        '''
        Samtools style region. 1-based indexing including both end-points.
        '''
        chrom, coords = region.split(':')

        beg, end = coords.split('-')

        beg = int(beg)

        end = int(end)

        beg = beg - 1

        df = self.extract_features_from_coords(chrom, beg, end, **kwargs)

        if df is None:
            return None

        df['coord'] = df['coord'] + 1

        return df

'''
Created on Dec 18, 2015

@author: andrew
'''
import os
import pandas as pd
import pysam
import yaml

from nuseq.features.extractors import NormalTumourFeatureExtractor
from utils import chunker, status_map


class DataSetBuilder(object):

    def __init__(self, config, out_file, store_position_information=False, target_features=None):
        self.config = config

        self._check_config()

        if not self._valid_config:
            raise Exception('Invalid configuration file')

        self.hdf_store = pd.HDFStore(out_file, mode='w', complevel=9, complib='blosc')

        self.store_position_information = store_position_information

        self.target_features = target_features

        self._position_cols = ['chrom', 'coord', 'ref_base', 'alt_base', 'normal', 'tumours', 'reference', 'status']

    def build_training_data(self):
        for sample in self.config['samples']:
            print 'Sample: {0}'.format(sample)

            sample_dir = os.path.join(self.config['base_dir'], sample)

            meta_file = os.path.join(sample_dir, 'meta.yaml')

            with open(meta_file) as fh:
                meta = yaml.load(fh)

            for run_config in meta:
                print 'Run: {0}'.format(run_config['run_id'])

                run_file = os.path.join(sample_dir, 'run_{0}.tsv'.format(run_config['run_id']))

                run_df = self._load_run_df(run_file)

                # Duplicates positions break the extractors so make unique
                original_shape = run_df.shape

                run_df = run_df.drop_duplicates(('chrom', 'coord'))

                if original_shape != run_df.shape:
                    print 'Duplicate entries detected. They will be removed.'

                self._append_labels(run_df)

                positions = [tuple(x) for x in run_df[['chrom', 'coord']].values]

                for split_index, split_positions in enumerate(chunker(positions, 1000)):
                    print 'Chunk: {0}'.format(split_index)

                    features_df = self._get_features_df(run_config, split_positions)

                    self._append_features(features_df)

                    if self.store_position_information:
                        self._append_positions(run_config, features_df, run_df)

    def close(self):
        self.hdf_store.close()

    def _append_features(self, df):
        if self.target_features is not None:
            cols = self.target_features

        else:
            cols = [x for x in df.columns if x not in self._position_cols]

        self.hdf_store.append('features', df[cols])

    def _append_labels(self, df):
        labels = []

        for status in df['status']:
            labels.append(status_map[status])

        self.hdf_store.append('labels', pd.Series(labels))

    def _append_positions(self, config, features_df, run_df):
        features_df['chrom'] = features_df['chrom'].astype(str)

        features_df['coord'] = features_df['coord'].astype(int)

        df = pd.merge(features_df, run_df, on=['chrom', 'coord'])

        df['normal'] = config['normal']

        df['tumours'] = ':'.join(config['tumours'])

        df['reference'] = config['reference']

        self.hdf_store.append(
            'positions',
            df[self._position_cols],
            min_itemsize=self._str_col_length
        )

    def _check_config(self):
        self._valid_config = True

        self._str_col_length = {
            'chrom': 0,
            'normal': 0,
            'reference': 0,
            'status': 0,
            'tumours': 0
        }

        for sample in self.config['samples']:
            sample_dir = os.path.join(self.config['base_dir'], sample)

            if not os.path.exists(sample_dir):
                print 'Sample directory {0} does not exist'.format(sample_dir)

                self._valid_config = False

                continue

            meta_file = os.path.join(sample_dir, 'meta.yaml')

            with open(meta_file) as fh:
                meta = yaml.load(fh)

            for run_config in meta:
                self._check_run_config(run_config, sample, sample_dir)

    def _check_run_config(self, run_config, sample, sample_dir):
        if not os.path.exists(run_config['reference']):
            print 'Reference genome {0} does not exist'.format(run_config['reference'])

            print 'Fix entry for sample {0} run {1}'.format(sample, run_config['run_id'])

            self._valid_config = False

        if not os.path.exists(run_config['normal']):
            print 'Normal BAM file {0} does not exist'.format(run_config['normal'])

            print 'Fix entry for sample {0} run {1}'.format(sample, run_config['run_id'])

            self._valid_config = False

        for file_name in run_config['tumours']:
            if not os.path.exists(file_name):
                print 'Tumour BAM file {0} does not exist'.format(file_name)

                print 'Fix entry for sample {0} run {1}'.format(sample, run_config['run_id'])

                self._valid_config = False

        for key in ['normal', 'reference']:
            self._str_col_length[key] = max(self._str_col_length[key], len(run_config[key]))

        self._str_col_length['tumours'] = max(self._str_col_length['tumours'], len(':'.join(run_config['tumours'])))

        run_file = os.path.join(sample_dir, 'run_{0}.tsv'.format(run_config['run_id']))

        if os.path.exists(run_file):
            run_df = self._load_run_df(run_file)

            self._str_col_length['chrom'] = max(self._str_col_length['chrom'], run_df['chrom'].str.len().max())

            self._str_col_length['status'] = max(self._str_col_length['status'], run_df['status'].str.len().max())

        else:
            print 'Run file {0} does not exist for sample {1}'.format(run_file, sample)

            self._valid_config = False

    def _get_features_df(self, config, positions):
        normal_bam = pysam.AlignmentFile(config['normal'])

        tumour_bams = []

        for file_name in config['tumours']:
            tumour_bams.append(pysam.AlignmentFile(file_name))

        ref_fasta = pysam.FastaFile(config['reference'])

        feature_extractor = NormalTumourFeatureExtractor(normal_bam, tumour_bams, ref_fasta)

        features_df = feature_extractor.extract_features_from_position_list(positions)

        normal_bam.close()

        for tumour_bam in tumour_bams:
            tumour_bam.close()

        ref_fasta.close()

        return features_df

    def _load_run_df(self, file_name):
        return pd.read_csv(file_name, converters={'chrom': str, 'coord': int}, sep='\t')

'''
Created on Nov 20, 2015

@author: Andrew Roth
'''
from collections import OrderedDict
from math import log10

import datetime
import StringIO
import vcf


class NuseqVcfWriter(object):
    nucleotides = ['A', 'C', 'G', 'T']

    def __init__(self, out_file, indel_threshold=0):
        self.indel_threshold = indel_threshold

        self.reader = vcf.Reader(StringIO.StringIO('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT'))

        self._init_filters()

        self._init_formats()

        self._init_infos()

        self._init_meta_data()

        self._init_samples()

        self._call_data_class = vcf.model.make_calldata_tuple(self.reader.formats.keys())

        self._format_string = ':'.join(self.reader.formats.keys())

        self._fh = open(out_file, 'w')

        self._writer = vcf.Writer(self._fh, self.reader)

    def _init_filters(self):
        filters = OrderedDict()

        filters['INDL'] = vcf.parser._Filter(
            id='INDL',
            desc='Proportion of reads spanning position with indels is >= {0}'.format(self.indel_threshold)
        )

        self.reader.filters = filters

    def _init_formats(self):
        formats = OrderedDict()

        formats['DP'] = vcf.parser._Format(
            id='DP',
            num=1,
            type='Integer',
            desc='Read depth'
        )

        for base in self.nucleotides:
            formats['{0}C'.format(base)] = vcf.parser._Format(
                id='{0}C'.format(base),
                num=1,
                type='Integer',
                desc='Number of reads with {0}'.format(base)
            )

        self.reader.formats = formats

    def _init_infos(self):
        infos = OrderedDict()

        infos['PG'] = vcf.parser._Info(
            id='PG',
            num=1,
            type='Float',
            desc='Probability of germline variant',
            source=None,
            version=None,
        )

        infos['PI'] = vcf.parser._Info(
            id='PI',
            num=1,
            type='Float',
            desc='Probability of indel artifact',
            source=None,
            version=None,
        )

        infos['PS'] = vcf.parser._Info(
            id='PS',
            num=1,
            type='Float',
            desc='Probability of somatic variant',
            source=None,
            version=None,
        )

        infos['GERMLINE'] = vcf.parser._Info(
            id='GERMLINE',
            num=0,
            type='Flag',
            desc='Germline mutation with probability >= 0.5',
            source=None,
            version=None,
        )

        infos['INDEL'] = vcf.parser._Info(
            id='INDEL',
            num=0,
            type='Flag',
            desc='Indel artifact with probability >= 0.5',
            source=None,
            version=None,
        )

        infos['SOMATIC'] = vcf.parser._Info(
            id='SOMATIC',
            num=0,
            type='Flag',
            desc='Somatic mutation with probability >= 0.5',
            source=None,
            version=None,
        )

        self.reader.infos = infos

    def _init_meta_data(self):
        meta_data = OrderedDict()

        meta_data['fileformat'] = 'VCFv4.1'

        meta_data['fileDate'] = str(datetime.date.today()).replace('-', '')

        self.reader.metadata = meta_data

    def _init_samples(self):
        self.reader.samples = ['NORMAL', 'TUMOUR']

    def close(self):
        self._writer.close()

    def write_record(
        self,
        chrom,
        coord,
        ref_base,
        alt_base,
        normal_counts,
        tumour_counts,
        germline_probability,
        indel_probability,
        somatic_probability,
        indel_proportion
    ):

        qual = -10 * log10(1 - somatic_probability + 10e-6)

        qual = round(qual, 2)

        record = vcf.model._Record(
            chrom,
            coord,
            None,
            ref_base,
            [vcf.model._Substitution(alt_base), ],
            qual,
            None,
            {
                'PG': germline_probability,
                'PI': indel_probability,
                'PS': somatic_probability,
                'INDEL': indel_probability >= 0.5,
                'GERMLINE': germline_probability >= 0.5,
                'SOMATIC': somatic_probability >= 0.5
            },
            self._format_string,
            {'NORMAL': 0, 'TUMOUR': 1})

        normal_data = vcf.model._Call(
            record,
            'NORMAL',
            self._call_data_class(normal_counts.sum(), *normal_counts)
        )

        tumour_data = vcf.model._Call(
            record,
            'TUMOUR',
            self._call_data_class(tumour_counts.sum(), *tumour_counts)
        )

        if indel_proportion >= self.indel_threshold:
            record.FILTER = ['INDL', ]

        record.samples = [normal_data, tumour_data]

        self._writer.write_record(record)
